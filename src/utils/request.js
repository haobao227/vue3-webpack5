import axios from "axios";
import { userStore } from "@/stores/user";
import Cookie from 'js-cookie'
const store = userStore()
class HttpRequest {
    constructor() {
        this.baseURL = ''
        // this.baseURL = '/api';
        this.timeout = 6000;
        this.queue = {} // 专门用来维护请求队列的
    }

    setInterceptor(instance, url) {
        instance.interceptors.request.use((config) => {
            // 开启loading
            if (Object.keys(this.queue).length === 0) {
                // 开启全局loading
            }
            let token = localStorage.getItem('token');
            if(token) {
                // 每次请求都会携带一个token 访问服务器
                config.headers['token'] = token
                Cookie.set('token', token)
                store.setToken(token)
            }
            let CancelToken = axios.CancelToken;
            config.cancelToken = new CancelToken((c) => {
                store.setCancelToken(c); // 同步将取消方法存入到vuex中
            });
            this.queue[url] = true
            return config;  // 扩展请求的配置
        });

        instance.interceptors.response.use((res) => {
            delete this.queue[url];
            if (Object.keys(this.queue).length === 0) {
                 // 关闭全局loading
            }

            if (res.status === 200 || res.status === 201) {
                return res.data; // 接口里面配合 可以switchCase 状态
            } else {
                console.log(res);
                return Promise.reject(res.data); // 失败抛出异常即可
            }
        }, error => {
            delete this.queue[url];
            if (Object.keys(this.queue).length == 0) {
                // close loading
            }
            let data = error.response && error.response.data
            return Promise.reject(data);
        });
    }

    request(options) {
        let instance = axios.create();
        let config ={
            baseURL: this.baseURL,
            timeout: this.timeout,
            ...options
        }
        this.setInterceptor(instance, config.url);
        return instance(config)
    }

    get(url, params = {}, options) {
        let config = {
            url,
            method: 'get',
            params,
        };
        config = Object.assign(config, options);
        return this.request(config)
    }

    post(url, data = {}, params) {
        return this.request({
            url,
            method: 'post',
            params,
            data
        })
    }
    patch(url, data, params) {
        return this.request({
            url,
            method: 'patch',
            params,
            data
        })
    }
}



export default new HttpRequest
