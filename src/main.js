import { createApp } from 'vue'
// import ViewUIPlus from 'view-ui-plus'
import App from './App.vue'
import { createPinia } from 'pinia'
import router from './router'
import ElementPlus from 'element-plus'
// import 'element-plus/dist/index.css'
// import 'view-ui-plus/dist/styles/viewuiplus.css'
import { debounce, throttle } from './directive'
const app = createApp(App)
const pinia = createPinia()

app.directive('debounce', debounce)
app.directive('throttle', throttle)

app.config.errorHandler = (err, vm, info) => {
  console.log(`Error: ${err.toString()} \n Info: ${info} \n `)
}

app.use(pinia)
  .use(router)
  .use(ElementPlus)
  .mount('#app')
