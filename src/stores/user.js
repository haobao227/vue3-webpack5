import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
export const userStore = defineStore('user', () => {

    const token = ref(null)
    const requestToken = ref([])


    const setToken = (value) => {
        token.value = value
    }

    const setCancelToken = (value) => {
        requestToken.value = [...requestToken.value, value]
    }

    const clearCancelToken = () => {
        requestToken.value.forEach((c) => c())
        requestToken.value = []
    }

    const
    return {
        token,
        setToken,
        setCancelToken,
        clearCancelToken,
    }
})