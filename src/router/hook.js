export default {
    'has_login': async (to, from, next) => {
        let token = localStorage.getItem('token');
        if (token) {
            if (to.path === '/login') {
                next({path: '/index'})
            } else if (to.meta && to.meta.needLogin) {
                next()
            } else {
                // todo
                next()
            }
        } else {
            if (to.meta && to.meta.needLogin) {
                next({path: '/login'})
            } else {
                next()
            }
        }
    }

}