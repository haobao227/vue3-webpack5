import { createRouter, createWebHashHistory } from 'vue-router'
import Index from '@/views/index.vue'
// import hooks from './hook';

const routes = [
  {
    path: '/',
    name: 'Index',
    meta: {
      title: '首页'
    },
    component: Index
  },
  {
    path: '/index2',
    name: 'Index2',
    meta: {
      title: '首页'
    },
    component: () => import(/* webpackChunkName: 'index2' */'@/views/index2.vue')
  }

]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

// Object.values(hooks).forEach(hook => {
//     router.beforeEach(hook);
// });

export default router
