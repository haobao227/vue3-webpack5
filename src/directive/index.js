import { debounce } from './debouce'
import { throttle } from './throttle'

export { debounce, throttle }
