function _throttle (func, wait = 500) {
  let timer = null
  let previous = 0
  return function anonymous (...params) {
    const now = new Date()
    // 用传入的时间  和   (当前时间)-(上一次的时间比较) = 中间间隔的时间 作差比较
    const remaining = wait - (now - previous)
    if (remaining <= 0) {
      clearTimeout(timer)
      timer = null
      previous = now
      func.call(this, ...params)
    } else if (!timer) {
      // 兜底用的  如果在冷却的时间段内 又点击了一下  过后还会执行最后一次  此时time != null 所以多次点击只会延迟执行一次
      timer = setTimeout(() => {
        clearTimeout(timer)
        timer = null
        previous = new Date()
        func.call(this, ...params)
      }, remaining)
    }
  }
}

export const throttle = {
  beforeMount (el, binding) {
    const [func, timer, change] = binding.value
    el.addEventListener(change, _throttle(func, timer))
  }
}
