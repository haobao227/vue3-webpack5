// function _debounce(func, wait) {
//     let timer;
//     return function () {
//         let context = this;
//         let args = arguments;
//         if (timer) clearTimeout(timer);
//         timer = setTimeout(function () {
//             func.apply(context, args)
//         }, wait);
//     }
// }
export const debounce = {
  beforeMount (el, binding) {
    const [func, timer, change] = binding.value
    el.addEventListener(change, _debounce(func, timer))
  }
}
function _debounce (func, wait, immediate) {
  let timer
  return function () {
    const context = this
    const args = arguments
    if (timer) clearTimeout(timer)
    if (immediate) {
      var callNow = !timer
      timer = setTimeout(() => {
        timer = null
      }, wait)
      if (callNow) func.apply(context, args)
    } else {
      timer = setTimeout(function () {
        func.apply(context, args)
      }, wait)
    }
  }
}
