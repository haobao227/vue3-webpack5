const path = require('path');
const { VueLoaderPlugin } = require("vue-loader");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ProgressBarPlugin = require("progress-bar-webpack-plugin");
const ESLintPlugin = require('eslint-webpack-plugin')
const webpack = require('webpack');


const resolve = (dir) => { 
  return path.resolve(__dirname, dir)
}
const DEV = process.env.NODE_ENV === 'development'



module.exports = {
  context: resolve('../'),
  entry: {
    app: './src/main.js'
  },
  mode: 'development',
  devtool: DEV ? 'eval-cheap-module-source-map' : false,
  output: {
    filename: '[name].js',
    path: resolve('../dist'),
    // 异步代码打包名称
    chunkFilename: "[name].async-chunk.js",
  },
  resolve: {
    alias: {
      '@': resolve('../src')
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader?cacheDirectory',
        },
        include: resolve('../src'),
        exclude: /node_modules/
      },
      {
        test: /\.css$/i,
        use: [
          (DEV ? 'style-loader' : MiniCssExtractPlugin.loader),
          {
            loader: "css-loader",            
            options: {
              // 处理css代码中存在@import的问题
              importLoaders: 1
            }
          },
          "postcss-loader"
        ],
      },
      {
        test: /\.less$/,
        use: [
          (DEV ? 'style-loader' : MiniCssExtractPlugin.loader),
          'css-loader',
          'postcss-loader',
          'less-loader'
        ]
      },
      {
        test: /\.s[ca]ss/,
        use: [
          (DEV ? 'style-loader' : MiniCssExtractPlugin.loader),
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        type: 'asset',
        use: [{
          loader: 'image-webpack-loader',
          options: {
            // 最好是生产环境用
            disable: DEV,
            // 压缩jpeg,jpg
            mozjpeg: {
              quality: 70
            },
            // 压缩png 1-7 越大压缩越狠
            optipng: {
              optimizationLevel: 5,
            },
            gifsicle: {  // 优化gif
              interlaced: true
            },
            // 将 jpg 和 png 压缩成webp
            webp: {
              enabled: true,
              quality: 70
            }
          }
        }],
        generator: {
          filename: 'images/[hash:5][ext][query]'
        },
        parser: {
          dataUrlCondition: {
            maxSize: 30 * 1024 //  小于30kb的内联
          }
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        type: 'asset/resource',
        generator: {
          filename: 'font/[name].[hash:5].[ext]'
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        type: 'asset/resource',
        generator: {
          filename: 'media/[name].[hash:5].[ext]'
        }
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new ESLintPlugin({
      fix: true,
      // context: resolve('src'),
      extensions: ['.js', '.vue']
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name]/[contenthash:5].css',
      chunkFilename: 'css/[name]/[contenthash:5].css',
    }),
    new webpack.DefinePlugin({
      __VUE_OPTIONS_API__: JSON.stringify(false), 
      __VUE_PROD_DEVTOOLS__: JSON.stringify(false)
    }),
    new ProgressBarPlugin()
  ]
}