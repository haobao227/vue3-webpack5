const path = require('path')
const baseConfig = require('./webpack.base.js')
const { merge } = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const CompressWebpackPlugin = require('compression-webpack-plugin');
const SpeedMeasurePlugin = require("speed-measure-webpack-plugin");
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')
module.exports = merge(baseConfig, {
    mode: 'production',
    externals: {
        // key是import from后面的 value是三方库暴露出来的  
        'vue': 'Vue',
        'element-plus': 'ElementPlus'
        // 'view-ui-plus': 'ViewUIPlus'
    },
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    output: {
                      // 是否输出可读性较强的代码(保留空格和制表符)  
                      beautify: false,  
                      // 是否保留代码中的注释, 默认保留  
                      comments: true,
                    },
                    compress: {
                        warnings: false, // 关闭一些没用的警告
                        drop_console: true,
                        drop_debugger: true,
                        pure_funcs: ['console.log']
                    }
                },
                parallel: true, // 并发运行
            }),
            new CssMinimizerPlugin()
        ],
    },
    // cache: {
    //   type: 'filesystem',
    // },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './index.html',
            filename: 'index.html',
            inject: 'body',
            cdn:[
                'https://unpkg.com/vue@3',
                // 'https://unpkg.com/view-ui-plus'
                'https://unpkg.com/element-plus'
            ],
            favicon: './favicon.ico',
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeAttributeQuotes: true
            },
        }),
        // 开启gzip压缩
        new CompressWebpackPlugin({
           test: /\.js(\?.*)?$/i,
           algorithm: "gzip",
           minRatio: 0.8,
        }),
        new CopyWebpackPlugin({
            patterns: [
                {
                    // from: 要拷贝的 to: 要拷贝到的目标文件夹
                    from: path.resolve(__dirname, '../public/'),
                    to: path.resolve(__dirname, '../dist/')
                }
            ],
        }),
        // new BundleAnalyzerPlugin(),   
    ]
})