
const webpack = require('webpack')
const path = require('path')
const baseConfig = require('./webpack.base.js')
const { merge } = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = merge(baseConfig, {
    mode: 'development',
    devServer: {
        client: {
            logging: 'none',
            overlay: {
                errors: true,
                warnings: false,
            }, // 出现错误或警告时 全屏覆盖
            progress: true, // 显示进度条
        },
        hot: true,
        compress: true,
        host: 'localhost',
        port: 8080,
        static: {
           directory: path.resolve(__dirname, '../public'),
        }
        // proxy: {
        //     '/': {
        //         target: 'http://172.18.15.130:8070',
        //         secure: false,
        //         changeOrigin: true,
        //         pathRewrite: {
        //             '^/': ''
        //         }
        //     },
        // }
    },
    performance: {
      hints: 'warning',
    },
    stats: 'errors-warnings',
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
            // favicon: './favicon.ico',
            inject: true
          })
    ]
})